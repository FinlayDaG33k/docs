$(function() {
  $('.notification-button, .profile-button, .dropdown-settings').dropdown({
    inDuration: 300,
    outDuration: 225,
    constrainWidth: false,
    hover: true,
    gutter: 0,
    belowOrigin: true,
    alignment: 'right',
    stopPropagation: false
  });
  $('.modal').modal();
  $(".button-collapse").sideNav();
  $(".dropdown-trigger").dropdown();
  $(".button-collapse").sideNav();
});
