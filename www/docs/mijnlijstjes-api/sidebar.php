<div class="widget">
  <h4 class="widget-title">MijnLijstjes API</h4>
  <ul>
    <li><a href="<?= htmlentities(Craftsman\EzServer::GetProto()); ?>://<?= htmlentities(Craftsman\EzServer::GetHome()); ?>/docs/mijnlijstjes-api">Welcome</a>
    <li><a href="<?= htmlentities(Craftsman\EzServer::GetProto()); ?>://<?= htmlentities(Craftsman\EzServer::GetHome()); ?>/docs/mijnlijstjes-api/getting-started">Getting Started</a></li>
  </ul>
</div>
