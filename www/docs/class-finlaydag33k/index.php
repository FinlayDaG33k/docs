<h2>Welcome to Class-FinlayDaG33k's documentation!</h2>
<p>
Class-FinlayDaG33k is a library consisting of handy functions I often use, infact, this very documentation site uses it!<br />
This library has been a major speed-up in the development of my own projects, and I've already spotted some other uers out in the wild.<br />
It is currently in Alpha-stage and a lot might happen right now as I'm experimenting around a lot.
</p>
<p>
  If this is your first time using <code>Class-FinlayDaG33k</code>, I'd highly recommend looking at the <a href="<?= htmlentities(Craftsman\EzServer::GetProto()); ?>://<?= htmlentities(Craftsman\EzServer::GetHome()); ?>/docs/Class-FinlayDaG33k/getting-started">Getting Started</a> page first!
</p>

<div class="card-panel red lighten-4 red-text text-darken-4">
  <b>Deprecation!</b><br />
  Development of this project has stopped.<br />
  You can find updates at the <a href="<?= htmlentities(Craftsman\EzServer::GetProto()); ?>://<?= htmlentities(Craftsman\EzServer::GetHome()); ?>/docs/craftsman">Craftsman</a> project!
</div>