<div class="widget">
  <h4 class="widget-title">Class-FinlayDaG33k</h4>
  <ul>
    <li><a href="<?= htmlentities(Craftsman\EzServer::GetProto()); ?>://<?= htmlentities(Craftsman\EzServer::GetHome()); ?>/docs/Class-FinlayDaG33k">Welcome</a>
    <li><a href="<?= htmlentities(Craftsman\EzServer::GetProto()); ?>://<?= htmlentities(Craftsman\EzServer::GetHome()); ?>/docs/Class-FinlayDaG33k/getting-started">Getting Started</a></li>
    <li><a href="<?= htmlentities(Craftsman\EzServer::GetProto()); ?>://<?= htmlentities(Craftsman\EzServer::GetHome()); ?>/docs/Class-FinlayDaG33k/captcha">Captcha</a></li>
    <li><a href="<?= htmlentities(Craftsman\EzServer::GetProto()); ?>://<?= htmlentities(Craftsman\EzServer::GetHome()); ?>/docs/Class-FinlayDaG33k/crypto">Crypto</a></li>
    <li><a href="<?= htmlentities(Craftsman\EzServer::GetProto()); ?>://<?= htmlentities(Craftsman\EzServer::GetHome()); ?>/docs/Class-FinlayDaG33k/database">Database</a></li>
    <li><a href="<?= htmlentities(Craftsman\EzServer::GetProto()); ?>://<?= htmlentities(Craftsman\EzServer::GetHome()); ?>/docs/Class-FinlayDaG33k/ezserver">EzServer</a></li>
    <li><a href="<?= htmlentities(Craftsman\EzServer::GetProto()); ?>://<?= htmlentities(Craftsman\EzServer::GetHome()); ?>/docs/Class-FinlayDaG33k/files">Files</a></li>
    <li><a href="<?= htmlentities(Craftsman\EzServer::GetProto()); ?>://<?= htmlentities(Craftsman\EzServer::GetHome()); ?>/docs/Class-FinlayDaG33k/maps">Maps</a></li>
  </ul>
</div>
