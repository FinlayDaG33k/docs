<h2>Database</h2>
<p>
The <code>database</code> module is made to make some database-related stuff easier.<br />
At this moment, it <b>only</b> supports <code>MySQLi</code>, but I am planning on supporting <code>PDO</code> in the near future as well.
</p>

<div class="card-panel red lighten-4 red-text text-darken-4">
  <b>Deprecation Warning</b><br />
  This class will be deprecated in 7.0 and removed in 8.0.<br />
  I highly recommend using an ORM like Propel2 instead.
</div>




<h4 id="Connect"><u>mixed</u> Connect(array $config)</h4>
<p>
  Opens up a connection to a <code>MySQL</code>-database through <code>MySQLi</code>.
</p>
<h6>Example Usage</h6>
<p>
  <pre>
    <code>
      $MySQL_Config = array(<br />
        "Host" => "127.0.0.1",<br />
        "Username" => "FinlayDaG33k",<br />
        "Password" => "RGlkLXlvdS1yZWFsbHktcnVuLXRoaXMtdGhyb3VnaC1hbi1lbmNvZGVyPw==", // Password has to be encoded in base64!<br />
        "Database" => "Documentation"<br />
      );<br />
      $conn = $FinlayDaG33k->Database->Connect($MySQL_Config); // Open up a connection<br />
    </code>
  </pre>

  Returns a MySQLi object.<br />
  Nothing really special here.
</p>



<h4 id="PConnect"><u>mixed</u> PConnect(array $config)</h4>
<p>
  Opens up a persistent connection to a <code>MySQL</code>-database through <code>MySQLi</code>.<br />
  You can read more about persistent connectons <a href="http://php.net/manual/en/mysqli.persistconns.php" target="_blank">here</a>.
</p>
<h6>Example Usage</h6>
<p>
  <pre>
    <code>
      $MySQL_Config = array(<br />
        "Host" => "127.0.0.1",<br />
        "Username" => "FinlayDaG33k",<br />
        "Password" => "RGlkLXlvdS1yZWFsbHktcnVuLXRoaXMtdGhyb3VnaC1hbi1lbmNvZGVyPw==", // Password has to be encoded in base64!<br />
        "Database" => "Documentation"<br />
      );<br />
      $pconn = $FinlayDaG33k->Database->PConnect($MySQL_Config); // Open up a connection<br />
    </code>
  </pre>

  Returns a MySQLi object that is cached and can be re-used between clients.
</p>



<h4 id="Close"><u>bool</u> Close(array $config)</h4>
<p>
  Closes the MySQLi connection.<br />
  For performance's sake, only use this when you're really done with the connection!
</p>
<h6>Example Usage</h6>
<p>
  <pre>
    <code>
      $MySQL_Config = array(<br />
        "Host" => "127.0.0.1",<br />
        "Username" => "FinlayDaG33k",<br />
        "Password" => "RGlkLXlvdS1yZWFsbHktcnVuLXRoaXMtdGhyb3VnaC1hbi1lbmNvZGVyPw==", // Password has to be encoded in base64!<br />
        "Database" => "Documentation"<br />
      );<br />
      $conn = $FinlayDaG33k->Database->Connect($MySQL_Config); // Open up a connection<br />

      echo $FinlayDaG33k->Database->Close($conn);
    </code>
  </pre>

  outputs either <code>1</code> or <code>0</code> depending on success or not.
</p>
