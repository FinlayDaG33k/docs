<h2>EzServer</h2>
<p>
The <code>ezserver</code> module is intended to simplify making websites in general.<br />
When using this module, you can do stuff more consistently.
</p>



<h4 id="jsLog"><u>void</u> jsLog(string $message)</h4>
<p>
  Writes something to the Javascript console.<br />
  Best used in the <code>head</code> or <code>body</code> of the page.
</p>
<h6>Example Usage</h6>
<p>
  <pre>
    <code>
      $FinlayDaG33k->EzServer->jsLog("I will be send to the Javscript console!");
    </code>
  </pre>
</p>
<h6>Example Output</h6>
Open up your JavaScript console to see this one in action!
<?php
  $FinlayDaG33k->EzServer->jsLog("I will be send to the Javscript console!");
?>



<h4 id="getProto"><u>string</u> getProto()</h4>
<p>
  Check wether the client has send its request using HTTP or HTTPS
</p>
<h6>Example Usage</h6>
<p>
  <pre>
    <code>
      $proto = $FinlayDaG33k->EzServer->getProto();<br />
      var_dump($proto);
    </code>
  </pre>
</p>
<h6>Example Output</h6>
<pre>
  <code>
    <?php
      $proto = $FinlayDaG33k->EzServer->getProto();
      var_dump($proto);
    ?>
  </code>
</pre>




<h4 id="getMethod"><u>string</u> getMethod()</h4>
<p>
  Checks the method which has been used to send the request (eg. <code>GET</code> or <code>POST</code>)
</p>
<h6>Example Usage</h6>
<p>
  <pre>
    <code>
      $method = $FinlayDaG33k->EzServer->getMethod();<br />
      var_dump($method);
    </code>
  </pre>
  <b><i class="fa fa-lightbulb-o" aria-hidden="true"></i> This function currently only supports <code>GET</code> or <code>POST</code></b>
</p>
<h6>Example Output</h6>
<pre>
  <code>
    <?php
      $method = $FinlayDaG33k->EzServer->getMethod();
      var_dump($method);
    ?>
  </code>
</pre>



<h4 id="getHome"><u>string</u> getHome();</h4>
<p>
  Get the home path of the current file.
</p>
<h6>Example Usage</h6>
<p>
  <pre>
    <code>
      $fileHome = $FinlayDaG33k->EzServer->getHome();<br />
      var_dump($fileHome);
    </code>
  </pre>
</p>
<p>Example Output</p>
<pre>
  <code>
    <?php
      $fileHome = $FinlayDaG33k->EzServer->getHome();
      var_dump($fileHome);
    ?>
  </code>
</pre>



<h4 id="getRoot"><u>string</u> getRoot()</h4>
<p>
  Get the root of the current domain.<br />
  Does about the same as <code>getHome()</code>, but does not show any directories.
</p>
<h6>Example Usage</h6>
<p>
  <pre>
    <code>
      $siteRoot = $FinlayDaG33k->EzServer->getRoot();<br />
      var_dump($siteRoot);
    </code>
  </pre>
</p>
<p>Example Output</p>
<pre>
  <code>
    <?php
      $siteRoot = $FinlayDaG33k->EzServer->getRoot();
      var_dump($siteRoot);
    ?>
  </code>
</pre>



<h4 id="getPage"><u>string</u> getPage([string $pageVar = "page"[, string $pageDir = "pages"[, string $defaultPage = "home"[, string $errorPage = "404"]]]])</h4>
<p>
  Checks wether the requested page exists in specified directory.<br />
  If no page has been requested, will show the default page.<br />
  If requested page does not exist, will show the error page.
</p>
<h6>Example Usage</h6>
<p>
  <pre>
    <code>
      $page = $FinlayDaG33k->EzServer->getPage("page","pages","home","404");<br />
      var_dump($page);
    </code>
  </pre>
</p>
<p>Example Output</p>
<pre>
  <code>
    <?php
    $page = $FinlayDaG33k->EzServer->getPage("page","pages","home","404");
    var_dump($page);
    ?>
  </code>
</pre>



<h4 id="randomStr"><u>string</u> randomStr(int $length = 8)</h4>
<p>
  returns a pseudo-random string of the specified length.
</p>
<h6>Example Usage</h6>
<p>
  <pre>
    <code>
      $randomString = $FinlayDaG33k->EzServer->randomStr(16);<br />
      var_dump($randomString);<br />
    </code>
  </pre>
</p>
<p>Example Output</p>
<pre>
  <code>
    <?php
      $randomString = $FinlayDaG33k->EzServer->randomStr(16);
      var_dump($randomString);
    ?>
  </code>
</pre>



<h4 id="strInsert"><u>string</u> strInsert(string $insert, int $pos, string $string)</h4>
<p>
  Inserts a string into another string at the specified position
</p>
<h6>Example Usage</h6>
<p>
  <pre>
    <code>
      $what = "quick brown fox jumps over the ";<br />
      $where = 4;<br />
      $string = "the lazy dog";<br />
      $complete = $FinlayDaG33k->EzServer->strInsert($what,$where,$string);<br />
      var_dump($complete);<br />
    </code>
  </pre>
</p>
<p>Example Output</p>
<pre>
  <code>
    <?php
      $what = "quick brown fox jumps over the ";
      $where = 4;
      $string = "the lazy dog";
      $complete = $FinlayDaG33k->EzServer->strInsert($what,$where,$string);
      var_dump($complete);
    ?>
  </code>
</pre>
