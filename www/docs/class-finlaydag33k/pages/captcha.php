<h2>Captcha</h2>
<p>
The <code>captcha</code> module is made aid in easily implementing the Google captcha in your website to prevent against bots.<br />
At this moment we only support Google captcha, but I have plans to support Coinhive captcha later down the road.<br />
Also do we only support the regular "I am not a robot" captcha, not the invisible kind (this will also be added later down the road).
</p>



<h4 id="initCaptcha"><u>void</u> initCaptcha(string $siteKey, string $onComplete)</h4>
<p>
  Adds the Google Captcha javascript to the website.<br />
  This function is best ran in the page head.<br />
  Automatically refreshes a Captcha on expiry.<br />
</p>
<h6>Example Usage</h6>
<p>
  <pre>
    <code>
      $FinlayDaG33k->Captcha->initCaptcha("somesitekey", "alert(\"Captcha has been submitted and is valid!\")");<br />
    </code>
  </pre>

  <b><i class="fa fa-lightbulb-o" aria-hidden="true"></i> This function adds Javascript code directly to the page!</b><br />
  <b><i class="fa fa-lightbulb-o" aria-hidden="true"></i> A sitekey can be obtained for free <a href="https://www.google.com/recaptcha/admin" target="_blank">here</a></b>
</p>
<h6>Example Output</h6>
Right-click -> inspect element to see this one!
<?php
  $FinlayDaG33k->Captcha->initCaptcha("6LdvQk8UAAAAALj06GeJtlYXMX8EhRgD0rZMFzul", "alert(\"Captcha has been submitted and is valid!\")");
?>



<h4 id="initCaptcha"><u>void</u> displayCaptcha()</h4>
<p>
  Adds a display div for the Captcha.
</p>
<h6>Example Usage</h6>
<p>
  <pre>
    <code>
      $FinlayDaG33k->Captcha->displayCaptcha();<br />
    </code>
  </pre>
  <b><i class="fa fa-lightbulb-o" aria-hidden="true"></i> This function requires <code>initCaptcha()</code> to be ran in the header!</a></b>
</p>
<h6>Example Output</h6>
<pre>
  <code>
    <?php
      $FinlayDaG33k->Captcha->displayCaptcha();
    ?>
  </code>
</pre>



<h4 id="initCaptcha"><u>bool</u> verifyCaptcha(string $siteSecret)</h4>
<p>
  Verifies the Captcha response.
</p>
<h6>Example Usage</h6>
<p>
  <pre>
    <code>
      echo $FinlayDaG33k->Captcha->verifyCaptcha("somesitesecret");<br />
    </code>
  </pre>
  This will output either <code>1</code> or <code>0</code> depending on if the captcha was valid or not.<br />
  <br />
</p>
<h6>Example Output</h6>
Unfortunately, there is no example for this one :\
