<h2>Crypto</h2>
<p>
The <code>crypto</code> module is made aid in <b>simple</b> cryptography.<br />
Right now, this module only supports AES-256-CBC, but this will be made more dynamic in the near future.
</p>



<h4 id="genIv"><u>mixed</u> genIv(int $length = 16)</h4>
<p>
  Generate a pseudo-random set of bytes.<br />
  can be used with <code>bin2hex()</code> to be turned into human-readable ASCII.
</p>
<h6>Example Usage</h6>
<p>
  <pre>
    <code>
      $iv = $FinlayDaG33k->Crypto->genIv(8);<br />
      var_dump(bin2hex($iv));
    </code>
  </pre>

  <b><i class="fa fa-lightbulb-o" aria-hidden="true"></i> This function currently requires <code>php 7.0</code> or newer!</b><br />
  <b><i class="fa fa-lightbulb-o" aria-hidden="true"></i> Using this function with <code>bin2hex()</code> results in a string double the length of the specified length. This is intended!</b>
</p>
<h6>Example Output</h6>
<pre>
  <code>
    <?php
      $iv = $FinlayDaG33k->Crypto->genIv(8);
      var_dump(bin2hex($iv));
    ?>
  </code>
</pre>



<h4 id="aesEncrypt"><u>string</u> aesEncrypt(string $value, string $key)</h4>
<p>
  Encrypt a value with a key using <code>aes-256-cbc</code>
  Automatically generates a random IV using <code>genIv()</code> and includes it in the base64 encoded string.
</p>
<h6>Example Usage</h6>
<p>
  <pre>
    <code>
      $message = "Wanna buy some sense of pride and accomplishment?";<br />
      $key = "Lootboxes-Are-Awesome!";<br />
      $encrypted = $FinlayDaG33k->Crypto->aesEncrypt($message,$key);<br />
      var_dump($encrypted);<br />
    </code>
  </pre>

  <b><i class="fa fa-lightbulb-o" aria-hidden="true"></i> This function currently requires <code>php 7.0</code> or newer!</b><br />
  <b><i class="fa fa-lightbulb-o" aria-hidden="true"></i> This function currently only supports <code>aes-256-cbc</code>. I will add more cyphers in the near future!</b>
</p>
<h6>Example Output</h6>
<pre>
  <code>
    <?php
    $message = "Wanna buy some sense of pride and accomplishment?";
    $key = "Lootboxes-Are-Awesome!";
    $encrypted = $FinlayDaG33k->Crypto->aesEncrypt($message,$key);
    var_dump($encrypted);
    ?>
  </code>
</pre>



<h4 id="aesDecrypt"><u>string</u> aesDecrypt(string $data, string $key)</h4>
<p>
  Decrypt a message made by <code>aesEncrypt()</code>
</p>
<h6>Example Usage</h6>
<p>
  <pre>
    <code>
      $data = "<?= htmlentities($encrypted); ?>"<br />
      $key = "<?= htmlentities($key); ?>";<br />
      var_dump($FinlayDaG33k->Crypto->aesDecrypt($data,$key));<br />
    </code>
  </pre>

  <b><i class="fa fa-lightbulb-o" aria-hidden="true"></i> This function currently requires <code>php 7.0</code> or newer!</b><br />
  <b><i class="fa fa-lightbulb-o" aria-hidden="true"></i> This function currently only supports <code>aes-256-cbc</code>. I will add more cyphers in the near future!<br />
  <b><i class="fa fa-lightbulb-o" aria-hidden="true"></i> This function relies on the presence of the IV inside of the base64 encoded string!</b>
</p>
<h6>Example Output</h6>
<pre>
  <code>
    <?php
      $decrypted = $FinlayDaG33k->Crypto->aesDecrypt($encrypted,$key);
      var_dump($decrypted);
    ?>
  </code>
</pre>



<h4 id="generateKey"><u>string</u> generateKey(string $a, string $b, int $seed, string $seed2)</h4>
<p>
  Derive a pseudo-random key from the given parameters.<br />
  Can be used with <code>aesEncrypt()</code>.
</p>
<h6>Example Usage</h6>
<p>
  <pre>
    <code>
      $key = $FinlayDaG33k->Crypto->generateKey("Turtles","Fishies",12345,"whatwhereyouexpectinghere?");<br />
      var_dump($key);
    </code>
  </pre>

  <b><i class="fa fa-lightbulb-o" aria-hidden="true"></i> The lenght of the type of the key will always be <code>string(64)</code> if everything goes right</b><br />
  <b><i class="fa fa-lightbulb-o" aria-hidden="true"></i> Do not set the <code>$seed</code> to change it every single time!.<br />
  <b><i class="fa fa-lightbulb-o" aria-hidden="true"></i> This function relies on the <code>EzServer</code> module</b><br />
</p>
<h6>Example Output</h6>
<pre>
  <code>
    <?php
      $key = $FinlayDaG33k->Crypto->generateKey("Turtles","Fishies",12345,"whatwhereyouexpectinghere?");
      var_dump($key);
    ?>
  </code>
</pre>
