<h2>Getting Started</h2>
Getting started with Class-FinlayDaG33k is very simple.<br />
While you can run Class-FinlayDaG33k on <code>PHP 5.6</code>, I do recommend running it on <code>PHP 7.0</code> or above.<br />
Unless you like bugs, in which case, please, go ahead!<br />

<p>
  First, download the latest version of Class-FinlayDaG33k:
  <pre>
    <code>
      git clone https://github.com/FinlayDaG33k/Class-FinlayDaG33k.git
    </code>
  </pre>

  Place the folder somewhere inside your project and include the <code>FinlayDaG33k.class.php</code> file:
  <pre><code>require_once("Class-FinlayDaG33k-master/FinlayDaG33k.class.php");</code></pre>

  That's it!<br />
  Class-FinlayDaG33k will automagically create an object which you can use!<br />
</p>
<p>
  Class-FinlayDaG33k is divided into smaller modules.<br />
  This makes it easier to hunt-down bugs if they do arrise, and keeps the code slightly easier to read.<br />
  By default, you can use the modules like so:
  <pre><code>$FinlayDaG33k->MODULENAME->FUNCTIONNAME()</code></pre>

  You can find a list of modules and their functions in the list on the left.<br />
  <b><i class="fa fa-lightbulb-o" aria-hidden="true"></i> Module and function names are case-sensitive!</b>
</p>
<p>
  When Class-FinlayDaG33k is loaded, it enables a function that checks the PHP Version and the Class-FinlayDaG33k version.<br />
  This can be outputted to the javascript console by calling this function (preferably after opening the &lt;body&gt;-tag or just before closing it):
  <pre>
    <code>
      $FinlayDaG33k->showWarnings();
    </code>
  </pre>

  <b><i class="fa fa-lightbulb-o" aria-hidden="true"></i> Using this functionality is completely optional! You can decide to hide it for specific users or disable it all-together!</b>
</p>
