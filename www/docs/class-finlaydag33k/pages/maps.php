<h2>Maps</h2>
<p>
The <code>maps</code> module makes it easier to work with the Google Maps API.
This class is brand-spanking new, so it doesn't offer a lot yet.
</p>



<h4 id="getDistance"><u>string</u> getDistance($origin,$destination[,$region=""[,$mode="driving"[,$units=0[,$key=""]]]])</h4>
<p>
  Get the distance between the Origin location and the Destination.<br />
</p>
<h6>Example Usage</h6>
<p>
  <pre>
    <code>
      $origin = "Westervoort";<br />
    	$destination = "Arnhem Station";<br />
    	$region = "NL";<br />
    	$mode = "biking";<br />
    	$units=0;<br />
    	var_dump($FinlayDaG33k->Maps->getDistance($origin,$destination,$region,$mode,$units));
    </code>
  </pre>
</p>
<h6>Example Output</h6>
<pre>
  <code>
    <?php
      $origin = "Westervoort";
      $destination = "Arnhem Station";
      $region = "NL";
      $mode = "biking";
      $units=0;
      var_dump($FinlayDaG33k->Maps->getDistance($origin,$destination,$region,$mode,$units));
    ?>
  </code>
</pre>

