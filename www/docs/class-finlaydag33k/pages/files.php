<h2>Files</h2>
<p>
The <code>files</code> module is made to make some file-related stuff easier.<br />
</p>

<h4 id="sizeToBinary"><u>string</u> sizeToBinary(int $bytes, array $units [, int $precision = 2])</h4>
<p>
  This function (though its name does not suggest this) is used to convert bytes to a more human-readable format.
  Divides the given amount of bytes by <code>1024</code>.
</p>
<h6>Example Usage</h6>
<p>
  <pre>
    <code>
      $totalBytes = 1572864;<br />
      $units = array("KiB","MiB","GiB","TiB");<br />
      var_dump($FinlayDaG33k->Files->sizeToBinary($totalBytes,$units,2));
    </code>
  </pre>
</p>
<h6>Example Output</h6>
<pre>
  <code>
    <?php
      $totalBytes = 1572864;
      $units = array("KiB","MiB","GiB","TiB");
      var_dump($FinlayDaG33k->Files->sizeToBinary($totalBytes,$units,2));
    ?>
  </code>
</pre>

<h4 id="sizeToSI"><u>string</u> sizeToSI(int $bytes, array $units [, int $precision = 2])</h4>
<p>
  Does the Does the same as <code>sizeToBinary()</code>, but instead of dividing by <code>1024</code>, it divides by <code>1000</code>.
</p>
<h6>Example Usage</h6>
<p>
  <pre>
    <code>
      $totalBytes = 1572864;<br />
      $units = array("KB","MB","GB","TB");<br />
      var_dump($FinlayDaG33k->Files->sizeToSI($totalBytes,$units,2));<br />
    </code>
  </pre>
</p>
<pre>
  <code>
    <?php
      $totalBytes = 1572864;
      $units = array("KB","MB","GB","TB");
      var_dump($FinlayDaG33k->Files->sizeToSI($totalBytes,$units,2));
    ?>
  </code>
</pre>
