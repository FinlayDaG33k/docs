<!-- Meta -->
<meta name="author" value="Aroop Roelofs">
<meta name="viewport" content="width=device-width, initial-scale=1.0">

<!-- Prefetch DNS -->
<link rel="dns-prefetch" href="https://code.jquery.com">
<link rel="dns-prefetch" href="https://cdnjs.cloudflare.com">
<link rel="dns-prefetch" href="https://use.fontawesome.com">
<link rel="dns-prefetch" href="https://fonts.googleapis.com">

<!-- Stylesheets -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/css/materialize.min.css">
<link rel="stylesheet" type="text/css" href="//<?= htmlentities(Craftsman\EzServer::GetHome()); ?>/lib/css/style.css?cache=<?= htmlentities(Craftsman\EzServer::randomStr(4)); ?>">
<link rel="stylesheet" type="text/css" href="//<?= htmlentities(Craftsman\EzServer::GetHome()); ?>/lib/css/finlaydag33k-widgets.css">
<link rel="stylesheet" type="text/css" href="//<?= htmlentities(Craftsman\EzServer::GetHome()); ?>/lib/css/finlaydag33k-typography.css">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.10/css/all.css" integrity="sha384-+d0P83n9kaQMCwj8F4RJB66tzIwOKmrdb46+porD/OvrJ+37WqIM7UoBtwHO6Nlg" crossorigin="anonymous">

<!-- Scripts -->
<script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/js/materialize.min.js"></script>
<script src="/cdn-cgi/apps/head/zoCnNhv9eOozxux1stPTXHDm7BA.js"></script><link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
