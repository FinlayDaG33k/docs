<nav>
  <div class="nav-wrapper container">
    <a href="<?= htmlentities(Craftsman\EzServer::GetProto()); ?>://<?= htmlentities(Craftsman\EzServer::GetHome()); ?>/" class="brand-img img-responsive"><img src="//<?= htmlentities(Craftsman\EzServer::GetHome()); ?>/lib/img/logo.png"><span class="brand-logo">DOCS</span></a>
    <ul id="nav-mobile" class="right">
      <li>
        <a href="#" data-activates="mobile-nav" class="button-collapse"><i class="material-icons">menu</i></a>
      </li>
    </ul>
    <ul id="nav-mobile" class="right hide-on-med-and-down">
      <li><a href="<?= htmlentities(Craftsman\EzServer::GetProto()); ?>://<?= htmlentities(Craftsman\EzServer::GetHome()); ?>/projects">Projects</a></li>
      <li><a href="https://gitlab.com/FinlayDaG33k/docs"><i class="fas fa-code-branch"></i></a></li>
    </ul>
    <ul class="side-nav" id="mobile-nav">
      <li><a href="<?= htmlentities(Craftsman\EzServer::GetProto()); ?>://<?= htmlentities(Craftsman\EzServer::GetHome()); ?>/projects">Projects</a></li>
    </ul>
  </div>
</nav>