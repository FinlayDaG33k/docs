<?php
  if(file_exists(dirname(__DIR__)."/docs/".strtolower($_GET['proj']))){
    if(file_exists(dirname(__DIR__)."/docs/".strtolower($_GET['proj']."/index.php"))){
      if(file_exists(dirname(__DIR__)."/docs/".strtolower($_GET['proj']."/sidebar.php"))){
        ?>
          <div class="row">
            <aside class="col s12 m3 l3 finlaydag33k-sidebar sidebar-to-right" id="sidebar">
              <?php include(dirname(__DIR__)."/docs/".strtolower($_GET['proj'])."/sidebar.php"); ?>
            </aside>
            <div class="col s12 m9 l9">
              <section id="content">
                <article>
                  <?php
                    if(empty($_GET['doc-page'])){
                      include(dirname(__DIR__)."/docs/".strtolower($_GET['proj'])."/index.php");
                    }else{
                      $pageDir = dirname(__DIR__)."/docs/".strtolower($_GET['proj'])."/pages/";
                      include($pageDir . Craftsman\EzServer::GetPage("doc-page",$pageDir) . ".php");
                    }
                  ?>
                  <?php  ?>
                </article>
              </section>
            </div>
          </div>
        <?php
      }else{
        ?>
          <div class="row">
            <div class="col s12 m12 l12">
              <section id="content">
                <article>
                  <?php include(dirname(__DIR__)."/docs/".strtolower($_GET['proj'])."/index.php"); ?>
                </article>
              </section>
            </div>
          </div>
        <?php
      }

    }else{
      echo "Project exists, but does not have an index file.";
    }

  }else{
    echo "Project documentation does not exist!";
  }
