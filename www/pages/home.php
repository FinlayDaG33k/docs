<div class="row">
  <div class="col s12 m12 l12">
    <section id="content">
      <article>
        <h4>FinlayDaG33k Documentation</h4>
        <p>
          Welcome to my documentation site!<br />
          Here you'll find documentation about the projects that I have created!<br />
          While I may have some older projects around here, it might be that projects which I haven't touched in a long time since building this site are missing.<br />
          If you can't find what you need, I'd highly recommend trying to <a href="https://www.finlaydag33k.nl/contact/" target="_blank">contact me on my main website</a>.
        </p>
        <p>
          <b>
            This site is still heavily under development, so not everything might work as intended!<br />
            Please keep that in mind while reading the docs!<br />
            <a href="https://gitlab.com/FinlayDaG33k/docs">Feel free to contribute the docs on Gitlab</a>
          </b>
        </p>
      </article>
    </section>
  </div>
</div>
