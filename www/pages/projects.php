<div class="row">
  <div class="col s12 m12 l12">
    <table class="bordered responsive-table">
      <thead>
        <th>Project Name</th>
        <th>Language(s)</th>
        <th>Latest Version</th>
        <th>Read License</th>
        <th>Repository</th>
        <th>Docs Link</th>
      </thead>
      <tbody>
        <tr>
          <td>Class-FinlayDaG33k</td>
          <td>PHP</td>
          <td>
            <?php
              if(!$redis->exists("repodata Class-FinlayDaG33k")){
                $repoData = file_get_contents("https://raw.githubusercontent.com/FinlayDaG33k/Class-FinlayDaG33k/master/package.json");
                $redis->set("repodata Class-FinlayDaG33k",$repoData);
                $redis->expire("repodata Class-FinlayDaG33k", 1800);
              }else{
                $repoData = $redis->get("repodata Class-FinlayDaG33k");
              }
              $repoData = json_decode($repoData,1);
              echo htmlentities($repoData['version']);
            ?>
          </td>
          <td><a href="https://raw.githubusercontent.com/FinlayDaG33k/Class-FinlayDaG33k/master/LICENSE" target="_blank">Click</a></td>
          <td><a href="https://github.com/FinlayDaG33k/Class-FinlayDaG33k" target="_blank">Click</a></td>
          <td><a href="<?= htmlentities(Craftsman\EzServer::GetProto()); ?>://<?= htmlentities(Craftsman\EzServer::GetHome()); ?>/docs/Class-FinlayDaG33k">Click</a></td>
        </tr>
        <tr>
          <td>MijnLijstjes API</td>
          <td>JSON</td>
          <td>
            N/A
          </td>
          <td>N/A</td>
          <td>N/A</td>
          <td><a href="<?= htmlentities(Craftsman\EzServer::GetProto()); ?>://<?= htmlentities(Craftsman\EzServer::GetHome()); ?>/docs/mijnlijstjes-api">Click</a></td>
        </tr>
      </tbody>
    </table>
  </div>
</div>
