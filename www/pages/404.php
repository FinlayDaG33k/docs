<div class="card-panel red lighten-4 red-text text-darken-4">
  <b>Whoops!</b>
  The page you tried to visit does not exist!
</div>