<?php
  require_once __DIR__ . '/vendor/autoload.php'; // Load the composer stuff
  $pageDir = "pages";

  Predis\Autoloader::register();
  $redis = new Predis\Client([
    'scheme' => 'tcp',
    'host'   => 'redisserver',
    'port'   => 6379,
  ]);
?>

<html>
  <head>
    <?php require_once("components/head.php"); ?>
  </head>
  <body>
    <header>
      <?php require_once("components/nav.php"); ?>
    </header>
    <main class="main-container">
      <div class="container">
        <?php include("{$pageDir}/" . Craftsman\EzServer::GetPage() . ".php"); ?>
      </div>
    </main>
    <footer>
    </footer>
    <script src="<?= htmlentities(Craftsman\EzServer::GetHome()); ?>/lib/js/index.js"></script>
  </body>
<html>
